//$(function () {
//    $('a[href*=#]:not([href=#])').click(function () {
//        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
//            var target = $(this.hash);
//            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
//            $('li.active').removeClass('active');
//            $(this).parent().addClass('active');
//            if (target.length) {
//                $('html,body').animate({
//                    scrollTop: target.offset().top
//                }, 1000);
//                return false;
//            }
//        }
//    });
//});

///**
// * downloadAsPDF    Trigers a PDF download event. The PDF will contain the contents of the
// *                  DOM element whose ID is passed as @contents.
// *
// * @param contents  ID of the element to be included in the PDF document.
// *
// * @name downloadAsPDF
// */
//function downloadAsPDF(contents){
//    /**
//     * jsPDF: Creates new jsPDF document object instance.
//     *
//     * @param orientation   One of "portrait" or "landscape" (or shortcuts "p" (Default), "l")
//     *
//     * @param unit          Measurement unit to be used when coordinates are specified.
//     *                      One of "pt" (points), "mm" (Default), "cm", "in".
//     *
//     * @param format        One of 'pageFormats' as shown below, default: a4
//     *
//     * @returns {jsPDF}
//     *
//     * @name jsPDF
//     */
//    var pdf = new jsPDF('p', 'pt', 'a4')
//
//    // source can be HTML-formatted string, or a reference
//    // to an actual DOM element from which the text will be scraped.
//    , source = $('#' + contents)[0]
//
//    // we support special element handlers. Register them with jQuery-style
//    // ID selector for either ID or node name. ("#iAmID", "div", "span" etc.)
//    // There is no support for any other type of selectors
//    // (class, of compound) at this time.
//    , specialElementHandlers = {
//        // element with id of "bypass" - jQuery style selector
//        '#bypassme': function (element, renderer) {
//            // true = "handled elsewhere, bypass text extraction"
//            return true
//        }
//    },
//
//    margins = {
//        top: 40,
//        bottom: 60,
//        left: 40,
//        width: 522
//    };
//
//    // all coords and widths are in jsPDF instance's declared units
//    // 'inches' in this case
//    pdf.fromHTML(
//        source  // HTML string or DOM elem ref.
//        , margins.left  // x coord
//        , margins.top   // y coord
//        , {
//            'width': margins.width // max width of content on PDF
//            , 'elementHandlers': specialElementHandlers
//        },
//
//        //dispose: object with X, Y of the last line add to the PDF
//        //         this allow the insertion of new lines after html
//        function (dispose) {
//            pdf.save('biofuels.pdf');
//        },
//        margins
//    );
//
//}

$(function () {
    $('[data-toggle="tooltip"]').tooltip()
});



$(document).ready(function() {
    $('#fullpage').fullpage({
        //Navigation
        menu: '#menu',
        lockAnchors: false,
        //anchors:['biofuels', 'introduction', 'current_stance', 'algae', 'genetics'],
        navigation: true,
        navigationPosition: 'right',
        navigationTooltips: ['Biofuels', 'Introduction', 'Current Stance', 'Algae', 'Genetics', 'Research', 'Conclusion', 'References', 'About'],
        showActiveTooltip: false,
        slidesNavigation: true,
        slidesNavPosition: 'top',

        //Scrolling
        css3: true,
        scrollingSpeed: 700,
        autoScrolling: true,
        fitToSection: true,
        fitToSectionDelay: 1000,
        scrollBar: false,
        easing: 'easeInOutCubic',
        easingcss3: 'ease',
        loopBottom: false,
        loopTop: false,
        loopHorizontal: false,
        continuousVertical: false,
        normalScrollElements: '#element1, .element2',
        scrollOverflow: false,
        touchSensitivity: 15,
        normalScrollElementTouchThreshold: 5,

        //Accessibility
        keyboardScrolling: true,
        animateAnchor: true,
        recordHistory: true,

        //Design
        controlArrows: true,
        verticalCentered: true,
        resize : false,
        sectionsColor : ['#fff', '#D0D13A', '#CBA6F9', '#DEFFD2', '#d9ccff', '#832C65', '#226764', '#AA6B39', '#D3A4EF'],
        paddingTop: '3em',
        paddingBottom: '10px',
        fixedElements: '#header, .footer',
        responsiveWidth: 0,
        responsiveHeight: 0,

        //Custom selectors
        sectionSelector: '.section',
        slideSelector: '.slide',

        //events
        onLeave: function(index, nextIndex, direction){},
        afterLoad: function(anchorLink, index){},
        afterRender: function(){},
        afterResize: function(){},
        afterSlideLoad: function(anchorLink, index, slideAnchor, slideIndex){},
        onSlideLeave: function(anchorLink, index, slideIndex, direction, nextSlideIndex){},

    });

});
