<?xml version="1.0"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html" doctype-system="about:legacy-compat"/>
    <xsl:template match="/contents">

        <html lang="en-UK">

            <!-- ............................................................................................................... -->

            <head>
                <meta charset="utf-8"/>
                <meta name="keywords" content="biofuels, sustainable energy, diesel, petrol, algae"/>
                <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

                <!-- Title, extracted from website/@title in the XML -->
                <title>
                    <xsl:value-of select="website/@title"/>
                </title>

                <!-- Stylesheet .................................................................................... -->
                <!-- Bootstrap 3 CSS -->
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"/>

                <!-- Custom font -->
                <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway"/>

                <!-- Custom CSS -->
                <link rel="stylesheet" href="./assets/css/custom.css"/>

                <!-- Font Awesome -->
                <link rel="stylesheet"
                      href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css"/>
                <!-- ............................................................................................... -->

                <!-- Begin Cookie Consent plugin by Silktide ....................................................... -->
                <!-- http://silktide.com/cookieconsent -->
                <script type="text/javascript">
                    window.cookieconsent_options = {
                    "message": "We use cookies for our cutting edge web technology " +
                    "to ensure the best experience on the website",
                    "dismiss": "Got it!",
                    "learnMore": "More info",
                    "link": "http://pouria.eu/coockies",
                    "theme": "dark-bottom"
                    };
                </script>

                <script type="text/javascript"
                        src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/1.0.9/cookieconsent.min.js"/>

                <!-- DO NOT MODIFY. -->
                <!--
                    The following script tag downloads a font from the Adobe Edge
                    Web Fonts server for use within the web page.
                -->
                <script type="text/javascript">
                    var __adobewebfontsappname__="dreamweaver"
                </script>
                <script src="http://use.edgefonts.net/josefin-sans:n1:default;ubuntu:n3:default.js"
                        type="text/javascript"/>

                <!--End of 'do not modify'-->

                <!-- ............................................................................................... -->

            </head>

            <!-- ................................................................................................... -->

            <body>
                <header class="hidden-print" id="header">
                    <div class="container-fluid">
                        <div class="row">
                            <img id="bk-img"
                                 class="img-responsive pull-right"
                                 src="assets/img/algae-lg.png"/>
                            <span class="biofuels"><i class="fa fa-tint" style="color: green"/>Biofuels.
                            </span>
                            <span class="header-disc">
                                <span style="color: #00410f">
                                    <i class="fa fa-tree"/>
                                    Environmental.
                                </span>
                                <br/>
                                <span style="color: #4E0000">
                                    <i class="fa fa-refresh"/>
                                    Sustainable.
                                </span>
                                <br/>
                                <span style="color: #121241">
                                    <i class="fa fa-check-circle-o"/>
                                    Feasible.
                                </span>
                            </span>
                            <img src="assets/img/algae-sep.png"
                                 style="bottom:0px;"/>

                        </div>
                    </div>

                    <!--Navigation bar ............................................................................. -->

                    <nav id="navbar" class="navbar navbar-default navbar-static navbar-fixed-top hidden-print">
                        <div class="container-fluid">
                            <div class="navbar-header">
                                <span class="navbar-brand navbar-link" href="#" style="color: lightgray">
                                    <span>
                                        <i class="fa fa-tint" style="color:green;"/>
                                        Biofuels
                                    </span>
                                </span>

                                <div id="navbar-opts">
                                    <div class="navbar-toggle collapsed"
                                         data-toggle="collapse"
                                         data-target="#navcol-1">
                                        <span class="icon-bar"/>
                                    </div>
                                </div>
                            </div>

                            <!--Menu-->
                            <div class="collapse navbar-collapse mnu">
                                <ul class="nav navbar-nav">

                                    <!--Reading menu items from the XML file-->
                                    <xsl:for-each select="section">
                                        <li role="presentation" class="nav-cnt">
                                            <xsl:element name="a">
                                                <xsl:attribute name="href">
                                                    #<xsl:value-of select="translate(@title, ' ','-')"/>
                                                </xsl:attribute>
                                                <xsl:attribute name="style">
                                                    color: yellowgreen;
                                                </xsl:attribute>
                                                <xsl:value-of select="@title"/>
                                            </xsl:element>
                                        </li>
                                    </xsl:for-each>

                                    <!--About item in the menu (not from XML)-->
                                    <li role="presentation">
                                        <a href="#about" style="color: yellowgreen;">About</a>
                                    </li>
                                </ul>

                                <!-- Right aligned navbar icons -->
                                <ul class="nav navbar-nav navbar-right">

                                    <!-- Make a PDF -->
                                    <li role="presentation"
                                        data-toggle="tooltip"
                                        data-placement="left"
                                        title="Beware: Generating a PDF takes a few seconds..."
                                        class="navbar-icons">

                                        <a href="javascript:downloadAsPDF('pageContents')">
                                            <span id="downloadPDF"
                                                  onclick="">
                                                <span class="small">PDF</span>
                                                <!-- From Awesome fonts -->
                                                <i class="fa fa-file-pdf-o fa-2x" style="color: red;"/>
                                            </span>
                                        </a>
                                    </li>

                                    <!-- Print the page -->
                                    <li role="presentation"
                                        class="navbar-icons">

                                        <a href="#about">
                                            <span id="downloadPDF"
                                                  onclick="javascript:window.print();">

                                                <!-- From Awesome fonts -->
                                                <i class="fa fa-print fa-2x" style="color: white;"/>
                                            </span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </nav>

                </header>

                <!-- ............................................................................................... -->


                <div id="pageContents" style="overflow=hidden; margin-top:500;">


                    <!-- ........................................................................................... -->


                    <!--Constructing sections from the XML file .................................................... -->

                    <!-- Iterating sections -->
                    <xsl:variable name="firstSec" select="true"/>
                    <xsl:for-each select="section">
                        <div class="scrollme">
                            <div class="animateme"
                                 data-when="span"
                                 data-easing="easeinout"
                                 data-from="0.4"
                                 data-to="0"
                                 data-opacity="0.5"
                                 data-translatex="-200">

                                <div class="container">
                                    <div class="row">
                                        <!-- Setting div IDs -->
                                        <xsl:element name="span">
                                            <xsl:attribute name="id">
                                                <xsl:value-of select="translate(@title, ' ','-')"/>
                                            </xsl:attribute>
                                        </xsl:element>
                                        <section class="col-md-12">

                                            <!-- Title of the section -->
                                            <h1>
                                                <xsl:value-of select="@title"/>
                                            </h1>

                                            <!-- Iterating paragraphs in the section -->
                                            <xsl:for-each select="current()/paragraph">
                                                <p>
                                                    <!-- Section paragraphs -->
                                                    <xsl:value-of select="current()" disable-output-escaping="yes"/>
                                                </p>

                                                <!-- Image of the paragraph -->
                                                <div class="col-sm-6 col-md-4">
                                                    <xsl:element name="img">
                                                        <xsl:attribute name="src">
                                                            <xsl:value-of select="image/@path"/>
                                                        </xsl:attribute>
                                                        <xsl:attribute name="class">
                                                            img-circle img-responsive
                                                        </xsl:attribute>
                                                    </xsl:element>
                                                </div>

                                            </xsl:for-each>
                                            <p>

                                            </p>

                                        </section>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </xsl:for-each>
                </div> <!-- pageContents -->

                <!-- ............................................................................................... -->

                <hr id="about"/>

                <!--About section ...................................................................................-->

                <div class="container ftr">
                    <div class="row">
                        <div class="scrollme">
                            <div class="animateme"
                                 data-when="span"
                                 data-easing="easeinout"
                                 data-from="1"
                                 data-to="0"
                                 data-rotatex="90">

                                <h3>About...</h3>

                                <!-- Website / column 1 -->
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <h5>Website.</h5>
                                    <xsl:value-of select="about/website" disable-output-escaping="yes"/>
                                </div>

                                <!-- Notices / column 2 -->
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 text-justify">
                                    <h5>Notices.</h5>
                                    <xsl:value-of select="about/notices" disable-output-escaping="yes"/>
                                </div>

                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">

                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <!-- ............................................................................................... -->

                <hr/>

                <!--Copyright Notices ...............................................................................-->

                <div class="col-md-6 pull-right" style="margin-bottom: 20px;">
                    <a href="https://opensource.org/">
                        <img class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-xs-3 img-responsive"
                             src="./assets/img/osi_logo_300X400_90ppi_0.png"/>
                    </a>
                    <p class="col-xl-10 col-lg-10 col-md-10 col-sm-10 col-xs-9 pull-right lead small">

                        Copyright &#169;  <!-- Copyright sign -->
                        <xsl:value-of select="copyright/@year"/>  <!-- Copyright year, extracted from the XML -->
                        &#160;  <!-- Space -->
                        <xsl:value-of select="copyright/@holder"/> <!-- Copyright holder, extracted from the XML -->
                        .
                        <br/>

                        The application of this work is licenced under
                        <a href="https://opensource.org/licenses/GPL-3.0">GLPv.2</a>
                        , an
                        <br/>
                        <a href="https://opensource.org/">Open Source Initiative</a>
                        approved licence.
                        <br/>

                        <!-- Check if repository information exists in the XML -->
                        <xsl:if test="copyright/@repository">
                            Download, clone or fork the source code from
                            <xsl:element name="a">
                                <xsl:attribute name="href">
                                    <xsl:value-of select="copyright/@repository"/>
                                </xsl:attribute>
                                this repository
                            </xsl:element>
                            .
                        </xsl:if>

                    </p>
                </div>

                <div class="col-md-6 pull-right">
                    <div class="pull-right" align="right">
                        <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">
                            <img alt="Creative Commons License"
                                 style="border-width:0"
                                 src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png"/>
                        </a>
                        <br/>

                        <p class="lead small">
                            This work by
                            <span xmlns:cc="http://creativecommons.org/ns#"
                                  property="cc:attributionName"
                                  rel="cc:attributionURL"
                                  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                                  xsi:schemaLocation="http://creativecommons.org/ns# ">

                                <xsl:value-of select="copyright/@holder"/>
                            </span>
                            is licensed under a
                            <br/>
                            <a rel="license"
                               href="http://creativecommons.org/licenses/by-sa/4.0/">
                                Creative Commons Attribution-ShareAlike 4.0 International License
                            </a>.
                        </p>
                    </div>
                </div>

                <!-- ............................................................................................... -->


                <!-- Loading JavaScript libraries .................................................................. -->
                <!-- JQuery  -->
                <script type="text/javascript"
                        src="http://code.jquery.com/jquery-1.11.3.min.js"/>
                <!-- Bootstrap  -->
                <!-- Latest compiled and minified JavaScript -->
                <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"
                        integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS"
                        crossorigin="anonymous"/>

                <!-- Scrollme -->
                <script type="text/javascript" src="./assets/js/jquery.scrollme.js"/>

                <!-- JSPDF -->
                <script type="text/javascript" src="./assets/js/jspdf.min.js"/>

                <!-- Website's own library -->
                <script type="text/javascript" src="./assets/js/custom.js"/>


                <!-- ............................................................................................... -->

            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>