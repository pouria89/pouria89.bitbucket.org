# Biofuels website
Version 1.8

> Author: Pouria Hadjibagheri  
> Start date: 2 December 2015
> Last modified: Sun  6 Dec 2015 12:18:49 GMT
> Licence: GPLv2.0

## Table of Contents
[TOC]

## Browser requirements
The use of cutting edge technology means that the website requires a modern browser to run properly. 
For more information on browsers and test yours, you can use [this test](https://html5test.com/index.html) 
provided by the [W3C (World Wide Web Consortium)](http://www.w3.org/). We recommend a browser that 
scores >450 in this test.

## Map
```
.  
├── _index.xhtml  
├── assets  
│   ├── css  
│   │   └── custom.css  
│   ├── img  
│   │   ├── algae.jpg  
│   │   ├── algae.png  
│   │   ├── osi_logo_300X400_90ppi_0.png  
│   │   └── osi_logo_600X800_90ppi_0.png  
│   └── js  
│       ├── custom.js  
│       ├── jquery.min.js  
│       ├── jquery.scrollme.js  
│       ├── jquery.scrollme.min.js  
│       ├── jspdf.js  
│       └── jspdf.min.js  
├── index.xml  
└── index.xsl   
```

## Programming
This is a frontend only, single page system website. It is mainly designed using HTML5 and CSS3. 
The data is retrived from an XML file, and then formatted through XSLT. Bootstrap 3 was used as 
the primary framework. JavaScript and JQuery also were employed for additional functionality.  
For information, please read the comments in the source code.

## Licence
Frontend only, single page system website, primarily designed for presenting a research proposal 
on the production of Biofuels.  

Copyright © 2015, Pouria Hadjibagheri.

This program is free software; you can redistribute it and/or modify it under the terms of 
the **GNU General Public License** as published by the Free Software Foundation; either 
Version 1.8
in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
Public License for more details.  

You should have received a copy of the GNU General Public License along with this program; 
if not, write to the Free Software Foundation, Inc., 51 Franklin Street, 
Fifth Floor, Boston, MA  02110-1301, USA; or visit [GPLv2.0](https://opensource.org/licenses/GPL-2.0).
